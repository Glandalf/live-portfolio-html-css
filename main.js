// On veut un tableau de tous nos diplômes
let diplomas = [{
    title: 'Dinde',
    date: '2019',
    description: 'coucou'
},
{
    title: 'Couverts',
    date: '2017',
    description: 'cool'
},
{
    title: 'Cochon',
    date: '2012',
    description: 'on sait pas'
}]

// On va afficher dans le HTML ces fameux diplômes
function displayDiplomas() {
    // On parcourt notre tableau de diplômes
    for(let diploma of diplomas) {
        // console.log('coucou', diploma.title)
        
        let htmlDiploma = `<li class="diploma">
        <h2 class="title">${diploma.title}</h2>
        <span class="date">${diploma.date}</span>
        <p class="description">${diploma.description}</p>
    </li>`
    document.querySelector('#education ul').innerHTML += htmlDiploma
    }

}

// On "lie" notre fonction à l'événement de click sur le bouton :
document.getElementById('show-diplomas').addEventListener('click', displayDiplomas)
